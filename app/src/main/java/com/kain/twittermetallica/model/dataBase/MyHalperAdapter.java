package com.kain.twittermetallica.model.dataBase;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyHalperAdapter {

    TwitterHalper twitterHalper;
    SQLiteDatabase db;

    private static final String DATABASE_NAME = "twitterdb";
    private static final String TABLE_NAME = "mettalica";
    private static final int DATABASE_VERSION = 1;
    private static final String UID = "_id";
    public static final String TWITT_TEXT = "twittertext";
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TWITT_TEXT
            + " text);";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public MyHalperAdapter(Context context) {
        twitterHalper = new TwitterHalper(context);
    }

    public void open() {
        db = twitterHalper.getWritableDatabase();
    }

    public long insertData(String Twittext) {
        db = twitterHalper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TWITT_TEXT, Twittext);
        long id = db.insert(TABLE_NAME, null, contentValues);
        return id;
    }

    public void close() {
        if (twitterHalper != null)
            twitterHalper.close();
    }

    public Cursor getAllData() {
        return db.query(TABLE_NAME, null, null, null, null, null, null);
    }

    static class TwitterHalper extends SQLiteOpenHelper {

        private Context context;

        public TwitterHalper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
            Message.maessage(context, "constructor called");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
            Message.maessage(context, "onCreate called");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Message.maessage(context, "onUpgrade called");
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }
    }
}
