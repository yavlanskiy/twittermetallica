package com.kain.twittermetallica.gui;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import com.google.gson.Gson;
import com.kain.twittermetallica.controller.R;
import com.kain.twittermetallica.model.Authenticated;
import com.kain.twittermetallica.model.Constants;
import com.kain.twittermetallica.model.WorkWithTwitter.Tweet;
import com.kain.twittermetallica.model.WorkWithTwitter.Twitter;
import com.kain.twittermetallica.model.dataBase.Message;
import com.kain.twittermetallica.model.dataBase.MyHalperAdapter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.*;
import java.net.URLEncoder;


public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    final static String LOG_TAG = "mLog";
    MyHalperAdapter myHalperAdapter;
    SimpleCursorAdapter scAdapter;
    ListView twitterList;
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twit_list);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorScheme(R.color.red, R.color.blue, R.color.grean, R.color.magenta);


        twitterList = (ListView) findViewById(R.id.twitList);
        myHalperAdapter = new MyHalperAdapter(this);
        myHalperAdapter.open();


        downloadTweets();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myHalperAdapter.close();
    }

    public void downloadTweets() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadTwitterTask().execute(Constants.SCREAAN_NAME);
        } else {
            Log.v(LOG_TAG, "No network connection available.");
            Message.maessage(getApplicationContext(), "Нет подключения к сети");

            String[] from = new String[]{MyHalperAdapter.TWITT_TEXT};
            int[] to = new int[]{R.id.tvText};

            scAdapter = new SimpleCursorAdapter(this, R.xml.item, null, from, to, 0);
            twitterList.setAdapter(scAdapter);
            getLoaderManager().initLoader(0, null, this);
        }
    }

    @Override
    public void onRefresh() {
        Toast.makeText(this, "Update", Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(true);
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(MainActivity.this, "Update completed", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    private class DownloadTwitterTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... screenNames) {
            String result = null;
            if (screenNames.length > 0) {
                result = getTwitterStream(screenNames[0]);
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            Twitter twits = jsonToTwitter(result);

            for (Tweet tweet : twits) {
                Log.i(LOG_TAG, tweet.getText() + " " + "DATE" + "--" + tweet.getDateCreated());
                String i = tweet.getText();
                myHalperAdapter.insertData(i);
            }

            ArrayAdapter<Tweet> adapter = new ArrayAdapter<Tweet>(getApplication(), android.R.layout.simple_list_item_1, twits);
            twitterList.setAdapter(adapter);
        }

        private Twitter jsonToTwitter(String result) {
            Twitter twits = null;
            if (result != null && result.length() > 0) {
                try {
                    Gson gson = new Gson();
                    twits = gson.fromJson(result, Twitter.class);
                } catch (IllegalStateException ex) {
                }
            }
            return twits;
        }

        private Authenticated jsonToAuthenticated(String rawAuthorization) {
            Authenticated auth = null;
            if (rawAuthorization != null && rawAuthorization.length() > 0) {
                try {
                    Gson gson = new Gson();
                    auth = gson.fromJson(rawAuthorization, Authenticated.class);
                } catch (IllegalStateException ex) {
                }
            }
            return auth;
        }

        private String getResponseBody(HttpRequestBase request) {
            StringBuilder sb = new StringBuilder();
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                String reason = response.getStatusLine().getReasonPhrase();

                if (statusCode == 200) {

                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    String line = null;
                    while ((line = bReader.readLine()) != null) {
                        sb.append(line);
                    }
                } else {
                    sb.append(reason);
                }
            } catch (UnsupportedEncodingException ex) {
            } catch (ClientProtocolException ex1) {
            } catch (IOException ex2) {
            }
            return sb.toString();
        }

        private String getTwitterStream(String screenName) {
            String results = null;

            try {
                String urlApiKey = URLEncoder.encode(Constants.CONSUMER_KEY, "UTF-8");
                String urlApiSecret = URLEncoder.encode(Constants.CONSUMER_SECRET, "UTF-8");

                String combined = urlApiKey + ":" + urlApiSecret;

                String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);

                HttpPost httpPost = new HttpPost(Constants.TWITTER_TOKEN_URL);
                httpPost.setHeader("Authorization", "Basic " + base64Encoded);
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
                String rawAuthorization = getResponseBody(httpPost);
                Authenticated auth = jsonToAuthenticated(rawAuthorization);

                if (auth != null && auth.token_type.equals("bearer")) {

                    HttpGet httpGet = new HttpGet(Constants.TWITTER_STREAM_URL + screenName);

                    httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                    httpGet.setHeader("Content-Type", "application/json");
                    results = getResponseBody(httpGet);
                }
            } catch (UnsupportedEncodingException ex) {
            } catch (IllegalStateException ex1) {
            }
            return results;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(this, myHalperAdapter);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        scAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    static class MyCursorLoader extends CursorLoader {
        MyHalperAdapter db;

        public MyCursorLoader(Context context, MyHalperAdapter db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllData();
            return cursor;
        }
    }
}